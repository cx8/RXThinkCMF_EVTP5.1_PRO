<?php
// +----------------------------------------------------------------------
// | RXThinkCMF敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2023 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 RXThinkCMF并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: @牧羊人 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace app\admin\behavior;

use think\Env;

class InitApp
{

    /**
     * 执行行为 run方法是Behavior唯一的接口
     * @author 牧羊人
     * @since 2020/11/19
     */
    public function run()
    {
        // 初始化系统常量
        $this->initSystemConst();

        // 初始化数据库
        $this->initDbInfo();

    }

    /**
     * 初始化系统常量
     *
     * @author 牧羊人
     * @date 2019-04-24
     */
    private function initSystemConst()
    {
        // 基础常量
        define('THINK_PATH', \Env::get('think_path'));
        define('ROOT_PATH', \Env::get('root_path'));
        define('APP_PATH', \Env::get('app_path'));
        define('CONFIG_PATH', \Env::get('config_path'));
        define('ROUTE_PATH', \Env::get('route_path'));
        define('RUNTIME_PATH', \Env::get('runtime_path'));
        define('EXTEND_PATH', \Env::get('extend_path'));
        define('VENDOR_PATH', \Env::get('vendor_path'));
        define('PLUGIN_PATH', ROOT_PATH . 'plugins');
        define('PUBLIC_PATH', ROOT_PATH . 'public');

        // 附件常量
        define('ATTACHMENT_PATH', PUBLIC_PATH . "/uploads");
        define('IMG_PATH', ATTACHMENT_PATH . "/images");
        define('UPLOAD_TEMP_PATH', ATTACHMENT_PATH . '/temp');
        define('IMG_URL', \Env::get('domain.image_url'));
    }

    /**
     * 初始化数据库
     * @author 牧羊人
     * @date 2019/4/23
     */
    private function initDbInfo()
    {
        // 数据表前缀
        define('DB_PREFIX', \Env::get('database.prefix'));
    }

}