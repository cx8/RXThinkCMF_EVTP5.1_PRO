<?php
// +----------------------------------------------------------------------
// | RXThinkCMF敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2023 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 RXThinkCMF并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: @牧羊人 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace app\admin\service;

use app\admin\model\User;

/**
 * 用户管理-服务类
 * @author 牧羊人
 * @since 2020/11/19
 * Class UserService
 * @package app\admin\service
 */
class UserService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2020/11/19
     * UserService constructor.
     */
    public function __construct()
    {
        $this->model = new User();
    }

    /**
     * 获取数据列表
     * @return array
     * @since 2020/11/19
     * @author 牧羊人
     */
    public function getList()
    {
        $param = request()->param();

        // 查询条件
        $map = [];

        // 用户账号
        $username = getter($param, "username");
        if ($username) {
            $map[] = ["username", 'like', "%{$username}%"];
        }
        // 用户姓名
        $realname = getter($param, "realname");
        if ($realname) {
            $map[] = ['realname', 'like', "%{$realname}%"];
        }
        // 用户性别
        $gender = getter($param, "gender");
        if ($gender) {
            $map[] = ['gender', '=', $gender];
        }
        return parent::getList($map, "id asc"); // TODO: Change the autogenerated stub
    }

    /**
     * 添加或编辑
     * @return mixed
     * @throws \think\db\exception\BindParamException
     * @throws \think\exception\PDOException
     * @since 2020/11/19
     * @author 牧羊人
     */
    public function edit()
    {
        // 请求参数
        $data = request()->param();
        // 用户名
        $username = trim($data['username']);
        // 密码
        $password = trim($data['password']);
        // 用户ID
        $id = getter($data, "id", 0);
        // 添加时设置密码
        if (empty($id)) {
            $data['password'] = get_password($password . $username);
            // 用户名重复性验证
            $count = $this->model
                ->where("username", '=', $username)
                ->where("mark", "=", 1)
                ->count();
            if ($count > 0) {
                return message("系统中已存在相同的用户名", false);
            }
        } else {
            // 用户名重复性验证
            $count = $this->model
                ->where("username", '=', $username)
                ->where("id", "<>", $data['id'])
                ->where("mark", "=", 1)
                ->count();
            if ($count > 0) {
                return message("系统中已存在相同的用户名", false);
            }
            // 获取用户信息
            $info = $this->model->getInfo($id);
            if (!$info) {
                return message("用户信息不存在", false);
            }
            $data['password'] = $info['password'];
        }

        // 头像处理
        $avatar = getter($data, 'avatar');
        if (strpos($avatar, "temp")) {
            $data['avatar'] = save_image($avatar, 'user');
        } else {
            $data['avatar'] = str_replace(IMG_URL, "", $data['avatar']);
        }

        // 头像处理
        $avatar = getter($data, 'avatar');
        if (strpos($avatar, "temp")) {
            $data['avatar'] = save_image($avatar, 'user');
        } else {
            $data['avatar'] = str_replace(IMG_URL, "", $data['avatar']);
        }

        // 出生日期
        if ($data['birthday']) {
            $data['birthday'] = strtotime($data['birthday']);
        }

        // 城市数据处理
        $city = $data['city'];
        if (!empty($city)) {
            $data['province_code'] = $city[0];
            $data['city_code'] = $city[1];
            $data['district_code'] = $city[2];
        } else {
            $data['province_code'] = 0;
            $data['city_code'] = 0;
            $data['district_code'] = 0;
        }
        unset($data['city']);

        $error = "";
        $result = $this->model->edit($data, $error);
        if (!$result) {
            return message($error, false);
        }

        // 删除用户整体缓存
        $this->model->cacheDAll();

        // 删除已存在的用户角色关系数据
        $userRoleService = new UserRoleService();
        $userRoleService->deleteUserRole($result);
        // 插入用户角色关系数据
        $userRoleService->insertUserRole($result, $data['role_ids']);
        return message();
    }

    /**
     * 获取用户信息
     * @param $userId
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @since 2020/11/19
     * @author 牧羊人
     */
    public function getUserInfo($userId)
    {
        $userInfo = $this->model->getInfo($userId);

        // 返回参数
        $result = array();
        $result['id'] = $userInfo['id'];
        $result['avatar'] = $userInfo['avatar'];
        $result['realname'] = $userInfo['realname'];
        $result['nickname'] = $userInfo['nickname'];
        $result['gender'] = $userInfo['gender'];
        $result['mobile'] = $userInfo['mobile'];
        $result['email'] = $userInfo['email'];
        $result['address'] = $userInfo['address'];
        $result['intro'] = $userInfo['intro'];
        $result['roles'] = [];
        $result['authorities'] = [];

        // 权限节点列表
        $menuService = new MenuService();
        $permissionList = $menuService->getPermissionsList($userId);
        $result['permissionList'] = $permissionList;
        return message("操作成功", true, $result);
    }

    /**
     * 更新个人资料
     * @param $userId 用户ID
     * @return array
     * @throws \think\db\exception\BindParamException
     * @throws \think\exception\PDOException
     * @author 牧羊人
     * @since 2021/4/23
     */
    public function updateUserInfo($userId)
    {
        // 参数
        $param = request()->param();
        // 个人信息
        $data = [
            'id' => $userId,
            'realname' => $param['realname'],
            'nickname' => $param['nickname'],
            'gender' => $param['gender'],
            'mobile' => $param['mobile'],
            'email' => $param['email'],
            'intro' => $param['intro'],
        ];
        // 头像处理
        $avatar = isset($param['avatar']) ? $param['avatar'] : "";
        if (strpos($avatar, "data:image") !== false) {
            $expData = explode(';', $avatar);
            $fileInfo = explode('/', $expData[0]);
            $fileExt = $fileInfo[1] == 'jpeg' ? 'jpg' : $fileInfo[1];
            // 文件存储路径
            $filePath = create_image_path("user", $fileExt);

            // 获取图片流
            $item = explode(',', $avatar);
            file_put_contents(ATTACHMENT_PATH . $filePath, base64_decode($item[1]));

            $data['avatar'] = $filePath;
        } else {
            $data['avatar'] = str_replace(IMG_URL, "", $param['avatar']);
        }
        $result = $this->model->edit($data);
        if (!$result) {
            return message("更新资料信息失败", false);
        }
        return message("更新资料信息成功");
    }

    /**
     * 修改密码
     * @param $userId 用户ID
     * @return mixed
     * @throws \think\db\exception\BindParamException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @since 2020/11/19
     * @author 牧羊人
     */
    public function updatePwd($userId)
    {
        // 获取参数
        $param = request()->param();
        // 原始密码
        $oldPassword = trim(getter($param, "oldPassword"));
        if (!$oldPassword) {
            return message("旧密码不能为空", false);
        }
        // 新密码
        $newPassword = trim(getter($param, "newPassword"));
        if (!$newPassword) {
            return message("新密码不能为空", false);
        }
        $userInfo = $this->model->getInfo($userId);
        if (!$userInfo) {
            return message("用户信息不存在", false);
        }
        if ($userInfo['password'] != get_password($oldPassword . $userInfo['username'])) {
            return message("旧密码输入不正确", false);
        }
        $item = [
            'id' => $userId,
            'password' => get_password($newPassword . $userInfo['username']),
        ];
        $result = $this->model->edit($item);
        if (!$result) {
            return message("修改失败", false);
        }
        return message("修改成功");
    }

    /**
     * 重置密码
     * @return mixed
     * @throws \think\db\exception\BindParamException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @author 牧羊人
     * @since 2020/11/19
     */
    public function resetPwd()
    {
        // 获取参数
        $param = request()->param();
        // 用户ID
        $userId = getter($param, "id");
        if (!$userId) {
            return message("用户ID不能为空", false);
        }
        $userInfo = $this->model->getInfo($userId);
        if (!$userInfo) {
            return message("用户信息不存在", false);
        }
        $item = [
            'id' => $param['id'],
            'password' => get_password("123456" . $userInfo['username']),
        ];
        $result = $this->model->edit($item);
        if (!$result) {
            return message("重置密码失败", false);
        }
        return message("重置密码成功");
    }

}